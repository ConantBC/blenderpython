'''Renders text from a CSV file to textures
and applies them to multiple objects.
'''

import codecs

def get_names(csv_filename):
    stream = codecs.open(csv_filename, 'r', 'utf-8-sig')
    return stream

def go():
    print(get_names('list2.csv'))
